var express = require('express');
var router = express.Router();
var util = require('util');
var md5 = require('md5');

const mysql = require('./mysql.js')
// var query = util.promisify(mysql.query)

function validateEmail(email) {

  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

function validateUserBody(req, res, next) {
  if (req.body?.first_name?.length > 3 && req.body?.last_name?.length < 255
    && req.body.last_name.length > 3 && req.body.last_name.length < 255
    && validateEmail(req.body.email)) 
  {
    next()
  } else {
    res.status(400).send('Invalid one or many fields')
  }
}

router.get('/users', function (req, res, next) {
  mysql.query('SELECT uuid, first_name, last_name, email, password FROM user_test', function (err, data) {
    if (err) return res.status(503).end('error');
    data.forEach(row => row.uuid = row.uuid.toString())
    res.json(data)
  })
});

router.get('/user/:uuid', function (req, res, next) {
  mysql.query('SELECT * FROM user_test WHERE uuid = ?', [req.params.uuid], function (err, data) {
    if (err) {
      return res.status(503).end('Something went wrong');
    }
    if (data?.length) {
      data[0].uuid = data[0]?.uuid?.toString();
      res.json(data[0])
    } else {
      res.json({})
    }
  })
});

router.post('/user/:uuid', validateUserBody, function (req, res, next) {
  mysql.query('UPDATE user_test SET first_name = ?, last_name = ?, email = ?, password = ? WHERE uuid = ?',
    [req.body.first_name, req.body.last_name, req.body.email, md5(req.body.password), req.params.uuid],
    function (err, data, ida) {
      console.log(err, data)
      if (err) return res.status(503).end('error')
      res.json(data)
    })
})

router.post('/user', validateUserBody, function (req, res, next) {
  mysql.query('INSERT INTO user_test (uuid, first_name, last_name, email, password) VALUES ((SELECT UUID()), ?, ?, ?, ?)',
    [req.body.first_name, req.body.last_name, req.body.email, md5(req.body.password)],
    function (err, data, ida) {
      if (err) return res.status(503).end('error')
      res.json(data)
    })
})

router.delete('/user/:id', function (req, res, next) {
  mysql.query('DELETE FROM user_test WHERE uuid = ?',
    [req.params.id],
    function (err, data, ida) {
      if (err) return res.status(503).end('error')
      res.json({ status: 'ok' })
    })
})

module.exports = router;
