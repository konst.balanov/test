import { createWebHistory, createRouter } from "vue-router";
import UsersList from "./../components/UsersList.vue";
import UserInfo from "./../components/UserInfo.vue";
import New from "./../components/New.vue";
import Edit from "./../components/Edit.vue";

const routes = [
  {
    path: "/",
    name: "Home",
    component: UsersList,
  },
  {
    path: "/user/:uuid",
    name: "User",
    component: UserInfo,
    props: (route) => { return { uuid: route.params.uuid } }
  },
  {
    path: "/new",
    name: "New",
    component: New,
  },
  {
    path: "/edit",
    name: "Edit",
    component: Edit,
    props: (route) => { return { uuid: route.params.uuid } }
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;