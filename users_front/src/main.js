import '@babel/polyfill'
import 'mutationobserver-shim'
import ElementPlus from 'element-plus';
import 'element-plus/lib/theme-chalk/index.css';
import Router from './plugins/router'
import { createApp } from 'vue'
import App from './App.vue'
import VueAxios from 'vue-axios'
import axios from 'axios'


axios.defaults.baseURL = process.env.NODE_ENV !== 'production' ? 'http://localhost:3000' : 'http://rebrotests.xyz/'
var app = createApp(App).use(ElementPlus).use(Router).use(VueAxios, axios).mount('#app')
