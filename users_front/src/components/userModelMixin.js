function validateEmail(email) {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

export default {
  props: ['uuid'],
  data() {
    return {
      first_name: "",
      last_name: "",
      email: "",
      password: "",
    }
  },
  computed: {
    validFirst_name() {
      return this.first_name.length > 3 && this.first_name.length < 255;
    },
    validLast_name() {
      return this.last_name.length > 3 && this.last_name.length < 255;
    },
    validEmail() {
      return validateEmail(this.email)
    },
    validFields() {
      return this.validFirst_name && this.validLast_name && this.validEmail;
    },
  },
}